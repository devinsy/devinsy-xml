  
# DEVINSY XML

DEVINSY XML provides an easy, local and fast Java classes to read/write XML files.

## Author
Christian Pierre MOMON &lt;christian.momon@devinsy.fr&gt;

## License
This software is released under the GNU LGPL.

## Requirements

- Java 1.6
- Eclipse Kepler 

## Context
Several useful Java classes.


## Conclusion
Enjoy and use DEVINSY XML. For questions, improvement, issues: christian.momon@devinsy.fr
	
