package foo;

/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.XMLEvent;

import fr.devinsy.util.xml.XMLTools;

/**
 * The Class Foo contains study code.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Foo
{
	/**
	 * The main method displays the XML event type reading a XML file
	 * 
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(final String args[]) throws Exception
	{
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader in = factory.createXMLEventReader(Foo.class.getResourceAsStream("/foo/foo.xml"));

		XMLEvent event;
		while (in.hasNext())
		{
			event = in.nextEvent();

			System.out.println(XMLTools.toString(event));
		}
	}
}
