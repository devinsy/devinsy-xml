/*
 * Copyright (C) 2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.StreamUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class XMLReaderTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XMLWriterTest
{
	public static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLWriterTest.class);

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);
	}

	/**
	 * Test foo 01.
	 * 
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 * @throws IOException
	 */
	@Test
	public void testtFoo01() throws XMLStreamException, XMLBadFormatException, IOException
	{
		logger.debug(StreamUtils.getBytes(XMLWriter.class.getResourceAsStream("/fr/devinsy/util/xml/foo01.xml")).length);

		//
		String source = new String(StreamUtils.getBytes(XMLWriter.class.getResourceAsStream("/fr/devinsy/util/xml/foo01.xml")));

		System.out.println("source=\n" + source);

		//
		XMLReader in = new XMLReader(source);

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		XMLWriter out = new XMLWriter(buffer);

		boolean ended = false;
		while (!ended)
		{
			XMLTag tag = in.readTag();

			if (tag == null)
			{
				ended = true;
			}
			else
			{
				out.writeTag(tag);
			}
		}
		out.close();

		String target = buffer.toString();

		System.out.println("=============");
		System.out.println("XML generated:");
		System.out.println(target);
		System.out.println(XMLTools.indent(target));
		System.out.println();
		System.out.println("Compare:");
		System.out.println(XMLTools.unindent(source));
		System.out.println(target);

		Assert.assertEquals(XMLTools.unindent(source), target);
	}
}
