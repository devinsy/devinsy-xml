/*
 * Copyright (C) 2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;

import fr.devinsy.util.strings.StringList;

/**
 * The Class XMLReaderTest.
 * 
 * @author Christian P. Momon
 */
public class XMLReaderTest
{
	private final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLReaderTest.class);

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ERROR);
	}

	/**
	 * Test foo 01.
	 * 
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	// @Test
	public void testFoo01() throws FileNotFoundException, XMLStreamException, XMLBadFormatException
	{
		//
		this.logger.debug("===== test starting...");

		// XMLReader in = new XMLReader(new
		// File("/home/cpm/C/Puck/TY/Ebrei 08.puc"));
		// XMLReader in = new XMLReader(new
		// File("/home/cpm/C/Puck/TY/T/kidarep.xml"));
		XMLReader in = new XMLReader(new File("/home/cpm/C/Puck/TY/T2/sikevadb-2014-06-08-17h55mn49s.xml"));

		boolean ended = false;
		while (!ended)
		{
			XMLTag tag = in.readTag();

			if (tag == null)
			{
				ended = true;
			}
			else
			{
				// System.out.println(String.format("tag %s", tag.getLabel()));
			}

			//
			this.logger.debug("===== test done.");
		}
		System.out.println("over");
	}

	/**
	 * Test foo 02.
	 * 
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	// @Test
	public void testFoo02() throws FileNotFoundException, XMLStreamException, XMLBadFormatException
	{
		//
		this.logger.debug("===== test starting...");

		// XMLReader in = new XMLReader(new
		// File("/home/cpm/C/Puck/TY/Ebrei 08.puc"));
		XMLReader in = new XMLReader(new File("/home/cpm/C/Puck/TY/T/accounts.xml"));
		// XMLReader in = new XMLReader(new
		// File("/home/cpm/C/Puck/TY/T2/sikevadb-2014-06-08-17h55mn49s.xml"));

		boolean ended = false;
		StringList buffer = new StringList();
		while (!ended)
		{
			XMLTag tag = in.readTag();

			if (tag == null)
			{
				ended = true;
			}
			else
			{
				if (tag.getContent() != null)
				{
					System.out.println(buffer.append(tag.getContent()));
				}
			}

			//
			this.logger.debug("===== test done.");
		}
		System.out.println("over");
	}
}
