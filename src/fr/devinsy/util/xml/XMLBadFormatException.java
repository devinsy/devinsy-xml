/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import org.slf4j.helpers.MessageFormatter;

/**
 * The Class XMLBadFormatException.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XMLBadFormatException extends Exception
{
	private static final long serialVersionUID = 768256303984176512L;

	/**
	 * Instantiates a new XML bad format exception.
	 * 
	 * @param message
	 *            the message
	 */
	public XMLBadFormatException(final String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new XML bad format exception.
	 * 
	 * @param message
	 *            the message
	 * @param exception
	 *            the exception
	 */
	public XMLBadFormatException(final String message, final Exception exception)
	{
		super(message, exception);
	}

	/**
	 * Instantiates a new XML bad format exception.
	 * 
	 * @param format
	 *            the format
	 * @param arguments
	 *            the arguments
	 */
	public XMLBadFormatException(final String format, final Object... arguments)
	{
		this(MessageFormatter.arrayFormat(format, arguments).getMessage());
	}

	/**
	 * Instantiates a new XML bad format exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public XMLBadFormatException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Instantiates a new XML bad format exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public XMLBadFormatException(final Throwable cause)
	{
		super(cause);
	}
}
