/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Reader;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.strings.StringList;
import fr.devinsy.util.xml.XMLTag.TagType;

/**
 * The Class XMLReader.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XMLReader
{
	private static final Logger logger = LoggerFactory.getLogger(XMLReader.class);

	protected XMLEventReader in;
	private XMLEvent nextEvent;

	/**
	 * Instantiates a new XML reader.
	 */
	protected XMLReader()
	{
		this.in = null;
		this.nextEvent = null;
	}

	/**
	 * Instantiates a new XML reader.
	 * 
	 * @param source
	 *            the file
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws XMLStreamException
	 *             the XML stream exception
	 */
	public XMLReader(final File source) throws FileNotFoundException, XMLStreamException
	{
		if (source == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.nextEvent = null;
			XMLInputFactory factory = XMLInputFactory.newInstance();
			this.in = factory.createXMLEventReader(new FileInputStream(source), "UTF-8");
		}
	}

	/**
	 * Instantiates a new XML reader.
	 * 
	 * @param source
	 *            the source
	 * @throws XMLStreamException
	 *             the XML stream exception
	 */
	public XMLReader(final InputStream source) throws XMLStreamException
	{
		if (source == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.nextEvent = null;
			XMLInputFactory factory = XMLInputFactory.newInstance();
			this.in = factory.createXMLEventReader(source);
		}
	}

	/**
	 * Instantiates a new XML reader.
	 * 
	 * @param source
	 *            the source
	 * @throws XMLStreamException
	 *             the XML stream exception
	 */
	public XMLReader(final Reader source) throws XMLStreamException
	{
		if (source == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.nextEvent = null;
			XMLInputFactory factory = XMLInputFactory.newInstance();
			this.in = factory.createXMLEventReader(source);
		}
	}

	/**
	 * @param source
	 * @throws XMLStreamException
	 */
	public XMLReader(final String source) throws XMLStreamException
	{
		if (source == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.nextEvent = null;
			XMLInputFactory factory = XMLInputFactory.newInstance();
			this.in = factory.createXMLEventReader(new ByteArrayInputStream(source.getBytes()));
		}
	}

	/**
	 * Close.
	 * 
	 * @throws XMLStreamException
	 *             the XML stream exception
	 */
	public void close() throws XMLStreamException
	{
		if (this.in != null)
		{
			this.in.close();
		}
	}

	/**
	 * This methods does a premonition act. Useful to detect end of a list.
	 * 
	 * @param label
	 *            the label
	 * @return true, if successful
	 * @throws XMLStreamException
	 *             the XML stream exception
	 */
	public boolean hasNextStartTag(final String label) throws XMLStreamException
	{
		boolean result;

		// Load next event.
		if (this.nextEvent == null)
		{
			if (this.in.hasNext())
			{
				this.nextEvent = this.in.nextEvent();
			}
		}

		// Analyze next event.
		if (this.nextEvent == null)
		{
			result = false;
		}
		else if ((this.nextEvent.isStartElement()) && (StringUtils.equals(this.nextEvent.asStartElement().getName().getLocalPart(), label)))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Read content tag.
	 * 
	 * @param label
	 *            the label
	 * @return the XML tag
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 * @throws XMLStreamException
	 *             the XML stream exception
	 */
	public XMLTag readContentTag(final String label) throws XMLBadFormatException, XMLStreamException
	{
		XMLTag result;

		//
		result = readTag();

		//
		if (result == null)
		{
			throw new XMLBadFormatException("XML file ends prematurely, content tag [" + label + "] is expected.");
		}
		else if (result.getType() != TagType.CONTENT)
		{
			throw new XMLBadFormatException("Content tag [" + label + "] is missing.");
		}
		else if (!StringUtils.equals(label, result.getLabel()))
		{
			throw new XMLBadFormatException("Tag with label [" + label + "] is missing.");
		}

		//
		return result;
	}

	/**
	 * Read end tag.
	 * 
	 * @param label
	 *            the label
	 * @return the XML tag
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public XMLTag readEndTag(final String label) throws XMLStreamException, XMLBadFormatException
	{
		XMLTag result;

		//
		result = readTag();

		//
		if (result == null)
		{
			throw new XMLBadFormatException("XML file ends prematurely, end tag [" + label + "] is expected.");
		}
		else if (result.getType() != TagType.END)
		{
			throw new XMLBadFormatException("End tag [" + label + "] is missing.");
		}
		else if (!StringUtils.equals(result.getLabel(), label))
		{
			throw new XMLBadFormatException("Tag with label [" + label + "] is missing.");
		}

		//
		return result;
	}

	/**
	 * Read list tag.
	 * 
	 * @param label
	 *            the label
	 * @return the XML tag
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public XMLTag readListTag(final String label) throws XMLStreamException, XMLBadFormatException
	{
		XMLTag result;

		//
		result = readTag();

		//
		if (result == null)
		{
			throw new XMLBadFormatException("XML file ends prematurely, tag [" + label + "] is expected.");
		}
		else if ((result.getType() != TagType.START) && (result.getType() != TagType.EMPTY))
		{
			throw new XMLBadFormatException("List tag [" + label + "] is missing.");
		}
		else if (!StringUtils.equals(label, result.getLabel()))
		{
			throw new XMLBadFormatException("Tag with label [" + label + "] is missing.");
		}

		//
		return result;
	}

	/**
	 * Read nullable content tag.
	 * 
	 * @param label
	 *            the label
	 * @return the XML tag
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public XMLTag readNullableContentTag(final String label) throws XMLStreamException, XMLBadFormatException
	{
		XMLTag result;

		//
		result = readTag();

		//
		if (result == null)
		{
			throw new XMLBadFormatException("XML file ends prematurely, tag [" + label + "] is expected.");
		}
		else if (!StringUtils.equals(label, result.getLabel()))
		{
			throw new XMLBadFormatException("Nullable content tag [" + label + "] is missing.");
		}
		else if ((result.getType() != TagType.EMPTY) && (result.getType() != TagType.CONTENT))
		{
			throw new XMLBadFormatException("Nullable content tag [" + label + "] is missing.");
		}

		//
		return result;
	}

	/**
	 * Read nullable start tag.
	 * 
	 * @param label
	 *            the label
	 * @return the XML tag
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public XMLTag readNullableStartTag(final String label) throws XMLStreamException, XMLBadFormatException
	{
		XMLTag result;

		//
		result = readTag();

		//
		if (result == null)
		{
			throw new XMLBadFormatException("XML file ends prematurely, start tag [" + label + "] is expected.");
		}
		else if ((result.getType() != TagType.START) && (result.getType() != TagType.EMPTY))
		{
			throw new XMLBadFormatException("Start tag [" + label + "] is missing.");
		}
		else if (!StringUtils.equals(result.getLabel(), label))
		{
			throw new XMLBadFormatException("Tag with label [" + label + "] is missing.");
		}

		//
		return result;
	}

	/**
	 * Read start tag.
	 * 
	 * @param label
	 *            the label
	 * @return the XML tag
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public XMLTag readStartTag(final String label) throws XMLStreamException, XMLBadFormatException
	{
		XMLTag result;

		//
		result = readTag();

		//
		if (result == null)
		{
			throw new XMLBadFormatException("XML file ends prematurely, start tag [" + label + "] is expected.");
		}
		else if (result.getType() != TagType.START)
		{
			throw new XMLBadFormatException("Start tag [" + label + "] is missing.");
		}
		else if (!StringUtils.equals(result.getLabel(), label))
		{
			throw new XMLBadFormatException("Tag with label [" + label + "] is missing.");
		}

		//
		return result;
	}

	/**
	 * Transducer graph :
	 * <ul>
	 * <li>START_DOCUMENT => HEADER TAG
	 * <li>START_ELEMENT(X) + START_ELEMENT(Y) => <X><Y> => START TAG
	 * <li>START_ELEMENT(X) + CHARACTERS(C) + START_ELEMENT(Y) => <X>SPACES<Y>=>
	 * START TAG
	 * <li>START_ELEMENT(X) + CHARACTERS(C) + END_ELEMENT(X) => <X>C</X> =>
	 * CONTENT TAG
	 * <li>START_ELEMENT(X) + END_ELEMENT(X) => <X></X> => <X/> => EMPTY
	 * <li>END_ELEMENT(X) => </X> => END TAG
	 * <li>END_DOCUMENT => FOOTER TAG
	 * </ul>
	 * .
	 * 
	 * @return the XML tag
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public XMLTag readTag() throws XMLStreamException, XMLBadFormatException
	{
		XMLTag result;

		int level = 1;
		boolean ended = false;
		result = null;
		XMLAttributes attributesBuffer = null;
		QName nameBuffer = null;
		StringList contentBuffer = null;
		while (!ended)
		{
			//
			XMLEvent event;
			if (this.nextEvent != null)
			{
				event = this.nextEvent;
				this.nextEvent = null;
			}
			else if (this.in.hasNext())
			{
				event = this.in.nextEvent();
			}
			else
			{
				event = null;
			}

			if (event == null)
			{
				ended = true;
				result = null;
			}
			else
			{
				logger.debug("eventType=" + XMLTools.toString(event));
				switch (level)
				{
					case 1:
						switch (event.getEventType())
						{
							case XMLEvent.START_DOCUMENT:
								// START_DOCUMENT => START DOCUMENT TAG
								ended = true;
								result = new XMLTag(null, TagType.HEADER, null);
							break;
							case XMLEvent.START_ELEMENT:
								// START_ELEMENT(X) => ...
								nameBuffer = event.asStartElement().getName();
								attributesBuffer = new XMLAttributes(event.asStartElement().getAttributes());
								level += 1;
							break;
							case XMLEvent.END_ELEMENT:
								// END_ELEMENT(X) => </X> => END TAG
								ended = true;
								result = new XMLTag(event.asEndElement().getName(), TagType.END, null);
							break;
							case XMLEvent.END_DOCUMENT:
								// END_DOCUMENT => END DOCUMENT TAG
								ended = true;
								result = new XMLTag(null, TagType.FOOTER, null);
							break;
						}
					break;
					case 2:
						switch (event.getEventType())
						{
							case XMLEvent.START_ELEMENT:
								// START_ELEMENT(X) + START_ELEMENT(Y) => <X><Y>
								// => START TAG
								ended = true;
								result = new XMLTag(nameBuffer, TagType.START, attributesBuffer);
								this.nextEvent = event;
							break;
							case XMLEvent.CHARACTERS:
								// START_ELEMENT(X) + CHARACTERS(C) => ...
								contentBuffer = new StringList(50);
								contentBuffer.append(event.asCharacters().getData());
								level += 1;
							break;
							case XMLEvent.END_ELEMENT:
								// START_ELEMENT(X) + END_ELEMENT(X) => <X></X>
								// => <X/> => EMPTY
								ended = true;
								result = new XMLTag(nameBuffer, TagType.EMPTY, attributesBuffer);
							break;
							default:
								throw new XMLBadFormatException("Unexpected XMLEvent [" + event.getEventType() + "].");
						}
					break;
					case 3:
						switch (event.getEventType())
						{
							case XMLEvent.START_ELEMENT:
								// START_ELEMENT(X) + CHARACTERS(C) +
								// START_ELEMENT(Y) =>
								// <X>SPACES<Y> => START TAG
								ended = true;
								result = new XMLTag(nameBuffer, TagType.START, attributesBuffer);
								this.nextEvent = event;
							break;
							case XMLEvent.CHARACTERS:
								// START_ELEMENT(X) + CHARACTERS(C1) +
								// CHARACTERS(C2)=> ...
								contentBuffer.append(event.asCharacters().getData());
							break;
							case XMLEvent.END_ELEMENT:
								// START_ELEMENT(X) + CHARACTERS(C) +
								// END_ELEMENT(X) => <X>C</X>
								// => CONTENT TAG
								ended = true;
								result = new XMLTag(nameBuffer, TagType.CONTENT, attributesBuffer);
								result.setContent(contentBuffer.toString());
							break;
							default:
								throw new XMLBadFormatException("Unexpected XMLEvent [" + event.getEventType() + "].");
						}
					break;
					default:
						throw new XMLBadFormatException("Unexpected level [" + level + "].");
				}
			}
		}

		logger.debug("=> " + XMLTools.toString(result));

		//
		return result;
	}

	/**
	 * Read XML footer.
	 * 
	 * @return the XML tag
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public XMLTag readXMLFooter() throws XMLStreamException, XMLBadFormatException
	{
		XMLTag result;

		//
		result = readTag();

		//
		if (result == null)
		{
			throw new XMLBadFormatException("XML file ends prematurely, end document event is expected.");
		}
		else if (result.getType() != TagType.FOOTER)
		{
			throw new XMLBadFormatException("End document tag is missing.");
		}

		//
		return result;
	}

	/**
	 * Read XML header.
	 * 
	 * @return the XML tag
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public XMLTag readXMLHeader() throws XMLStreamException, XMLBadFormatException
	{
		XMLTag result;

		//
		result = readTag();

		//
		if (result == null)
		{
			throw new XMLBadFormatException("XML file ends prematurely, start document event is expected.");
		}
		else if (result.getType() != TagType.HEADER)
		{
			throw new XMLBadFormatException("XML header is missing.");
		}

		//
		return result;
	}
}
