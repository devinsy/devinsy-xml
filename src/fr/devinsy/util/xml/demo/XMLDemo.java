/*
 * Copyright (C) 2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml.demo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.util.xml.XMLWriter;

/**
 * The Class XmlDemo.
 */
public class XMLDemo
{

	/**
	 * The main method of XMLDemo.
	 * 
	 * @param args
	 *            the arguments
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 */
	public static void main(final String[] args) throws IOException, XMLStreamException, XMLBadFormatException
	{
		//
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ERROR);

		//
		String xml;
		{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			XMLWriter out = new XMLWriter(stream);

			out.writeXMLHeader();

			//
			out.writeStartTag("account", "id", String.valueOf(77));
			{
				out.writeTag("email", "christian.momon@devinsy.fr");
				out.writeTag("first_names", "Christian Pierre");
				out.writeTag("last_name", "MOMON");
				out.writeTag("comment", "Foo character <moo> \" \'");
			}
			out.writeEndTag("account");

			//
			out.close();
			xml = stream.toString();
		}

		System.out.println("XML generated:");
		System.out.println(xml);
		System.out.println(XMLTools.indent(xml));
		System.out.println();

		{
			XMLReader in = new XMLReader(xml);

			in.readXMLHeader();

			//
			XMLTag tag = in.readStartTag("account");
			long id = Long.parseLong(tag.attributes().getByLabel("id").getValue());
			{
				String email = in.readContentTag("email").getContent();
				String firstName = in.readNullableContentTag("first_names").getContent();
				String lastName = in.readNullableContentTag("last_name").getContent();
				String comment = in.readNullableContentTag("comment").getContent();

				System.out.println("Data read:");
				System.out.println("Id=" + id);
				System.out.println("email=" + email);
				System.out.println("firstName=" + firstName);
				System.out.println("lastName=" + lastName);
				System.out.println("comment=" + comment);
			}
			in.readEndTag("account");

			//
			in.close();
		}
	}
}
