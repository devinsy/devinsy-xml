/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipInputStream;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.devinsy.util.strings.StringList;

/**
 * The Class XMLTools.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XMLTools
{
	/**
	 * Escape xml blank.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String escapeXmlBlank(final String source)
	{
		String result;

		if (StringUtils.isBlank(source))
		{
			result = "";
		}
		else
		{
			result = StringEscapeUtils.escapeXml(source);
		}

		//
		return result;
	}

	/**
	 * Indent.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 * @throws XMLStreamException
	 * @throws XMLBadFormatException
	 * @throws IOException
	 */
	public static String indent(final String source) throws XMLStreamException, XMLBadFormatException, IOException
	{
		String result;

		XMLReader in = new XMLReader(source);
		StringList buffer = new StringList();
		XMLWriter out = new XMLWriter(buffer);

		boolean ended = false;
		int level = 0;
		while (!ended)
		{
			XMLTag tag = in.readTag();

			if (tag == null)
			{
				ended = true;
			}
			else
			{
				switch (tag.getType())
				{
					case HEADER:
						out.writeXMLHeader(tag.attributes());
						out.flush();
						buffer.appendln();
					break;
					case START:
						out.flush();
						buffer.append(StringUtils.repeat('\t', level));
						out.writeStartTag(tag.getLabel(), tag.attributes());
						out.flush();
						buffer.appendln();
						level += 1;
					break;
					case CONTENT:
						out.flush();
						buffer.append(StringUtils.repeat('\t', level));
						out.writeTag(tag.getLabel(), tag.getContent(), tag.attributes());
						out.flush();
						buffer.appendln();
					break;
					case END:
						level -= 1;
						out.flush();
						buffer.append(StringUtils.repeat('\t', level));
						out.writeEndTag(tag.getLabel());
						out.flush();
						buffer.appendln();
					break;
					case EMPTY:
						out.flush();
						buffer.append(StringUtils.repeat('\t', level));
						out.writeEmptyTag(tag.getLabel(), tag.attributes());
						out.flush();
						buffer.appendln();
					break;
					case FOOTER:
					break;
				}
			}
		}

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * Checks if is valid.
	 * 
	 * @param xmlFile
	 *            the xml file
	 * @param xsdFile
	 *            the xsd file
	 * @return true, if is valid
	 * @throws SAXException
	 *             the SAX exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static boolean isValid(final File xmlFile, final File xsdFile) throws SAXException, IOException
	{
		boolean result;

		//
		InputStream xmlSource;
		if (isZipFile(xmlFile))
		{
			ZipInputStream zin = new ZipInputStream(new FileInputStream(xmlFile));
			zin.getNextEntry();
			xmlSource = zin;
		}
		else
		{
			xmlSource = new FileInputStream(xmlFile);
		}

		//
		result = isValid(xmlSource, new FileInputStream(xsdFile));

		//
		return result;
	}

	/**
	 * Checks if is valid.
	 * 
	 * @param xmlFile
	 *            the xml file
	 * @param xsdSource
	 *            the xsd source
	 * @return true, if is valid
	 * @throws SAXException
	 *             the SAX exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static boolean isValid(final File xmlFile, final InputStream xsdSource) throws SAXException, IOException
	{
		boolean result;

		//
		InputStream xmlSource;
		if (isZipFile(xmlFile))
		{
			ZipInputStream zin = new ZipInputStream(new FileInputStream(xmlFile));
			zin.getNextEntry();
			xmlSource = zin;
		}
		else
		{
			xmlSource = new FileInputStream(xmlFile);
		}

		//
		result = isValid(xmlSource, xsdSource);

		//
		return result;
	}

	/**
	 * Checks if is valid.
	 * 
	 * @param xmlSource
	 *            the xml source
	 * @param xsdSource
	 *            the xsd source
	 * @return true, if is valid
	 * @throws SAXException
	 *             the SAX exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static boolean isValid(final InputStream xmlSource, final InputStream xsdSource) throws SAXException, IOException
	{
		boolean result;

		if (xmlSource == null)
		{
			result = false;
		}
		else
		{
			try
			{
				//
				SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
				InputSource sourceentree = new InputSource(xsdSource);
				SAXSource sourceXSD = new SAXSource(sourceentree);
				Schema schema = factory.newSchema(sourceXSD);
				Validator validator = schema.newValidator();

				//
				validator.validate(new StreamSource(xmlSource));
				result = true;

			}
			catch (final IllegalArgumentException exception)
			{
				exception.printStackTrace();
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Checks if is zip file.
	 * 
	 * @param file
	 *            the file
	 * @return true, if is zip file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static boolean isZipFile(final File file) throws IOException
	{
		boolean result;

		//
		byte[] buffer = new byte[4];
		FileInputStream is = null;
		try
		{
			is = new FileInputStream(file);
			is.read(buffer);
		}
		finally
		{
			if (is != null)
			{
				is.close();
			}
		}

		// 50 4B 3 4
		if ((buffer[0] == 0x50) && (buffer[1] == 0x4B) && (buffer[2] == 0x03) && (buffer[3] == 0x04))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Read tag.
	 * 
	 * @param in
	 *            the in
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String readTag(final BufferedReader in) throws Exception
	{
		String result;

		Pattern TAG_PATTERN = Pattern.compile("^<([\\w-_\\.]+)>.*</([\\w-_\\.]+)>$");
		Pattern SHORT_TAG_PATTERN = Pattern.compile("^<.+/>$");

		result = in.readLine();
		boolean ended = false;
		while (!ended)
		{
			/*
			 * DEBUG Matcher tagMatcher2 = TAG_PATTERN.matcher(result); if
			 * (tagMatcher2.find()) { logger.info("group count,0,1,2 = [" +
			 * tagMatcher2.groupCount() + "][" + tagMatcher2.group(0) + "][" +
			 * tagMatcher2.group(1) + "][" + tagMatcher2.group(2) + "]"); }
			 */

			Matcher tagMatcher = TAG_PATTERN.matcher(result);
			Matcher shortTagMatcher = SHORT_TAG_PATTERN.matcher(result);

			if ((tagMatcher.find()) && (tagMatcher.groupCount() == 2) && (tagMatcher.group(1).equals(tagMatcher.group(2))))
			{
				ended = true;
			}
			else if (shortTagMatcher.matches())
			{
				ended = true;
			}
			else
			{
				result += in.readLine();
			}
		}

		//
		return result;
	}

	/**
	 * To HTLM 5.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toHTLM5(final String source)
	{
		String result;

		if (StringUtils.isBlank(source))
		{
			result = "";
		}
		else
		{
			result = source.replace("&nbsp;", "&#160;");
		}

		//
		return result;
	}

	/**
	 * To string.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toString(final XMLEvent source)
	{
		String result;

		switch (source.getEventType())
		{
			case XMLEvent.ATTRIBUTE:
				result = "ATTRIBUTE ";
			break;
			case XMLEvent.CDATA:
				result = "CDATA";
			break;
			case XMLEvent.CHARACTERS:
				result = String.format("CHARACTERS [%s]", source.asCharacters().getData());
			break;
			case XMLEvent.COMMENT:
				result = "COMMENT";
			break;
			case XMLEvent.DTD:
				result = "DTD";
			break;
			case XMLEvent.END_DOCUMENT:
				result = "END_DOCUMENT";
			break;
			case XMLEvent.END_ELEMENT:
				result = String.format("END_ELEMENT [%s]", source.asEndElement().getName());
			break;
			case XMLEvent.ENTITY_DECLARATION:
				result = "ENTITY_DECLARATION";
			break;
			case XMLEvent.ENTITY_REFERENCE:
				result = "ENTITY_REFERENCE";
			break;
			case XMLEvent.NAMESPACE:
				result = "NAMESPACE";
			break;
			case XMLEvent.NOTATION_DECLARATION:
				result = "NOTATION_DECLARATION";
			break;
			case XMLEvent.PROCESSING_INSTRUCTION:
				result = "PROCESSING_INSTRUCTION";
			break;
			case XMLEvent.SPACE:
				result = "SPACE";
			break;
			case XMLEvent.START_DOCUMENT:
				result = "START_DOCUMENT";
			break;
			case XMLEvent.START_ELEMENT:
				result = String.format("START_ELEMENT [name=%s][namespaceURI=%s][prefix=%s][localPart=%s]", source.asStartElement().getName(), source.asStartElement().getName().getNamespaceURI(),
						source.asStartElement().getName().getPrefix(), source.asStartElement().getName().getLocalPart());
			break;
			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * To string.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toString(final XMLTag source)
	{
		String result;

		if (source == null)
		{
			result = "null";
		}
		else
		{
			result = String.format("[label=%s][type=%s][content=%s]", source.getLabel(), source.getType().toString(), source.getContent());
		}

		//
		return result;
	}

	/**
	 * Unescape xml blank.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String unescapeXmlBlank(final String source)
	{
		String result;

		if (StringUtils.isBlank(source))
		{
			result = null;
		}
		else
		{
			result = StringEscapeUtils.unescapeXml(source);
		}

		//
		return result;
	}

	/**
	 * Unindent.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 * @throws XMLStreamException
	 *             the XML stream exception
	 * @throws XMLBadFormatException
	 *             the XML bad format exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String unindent(final String source) throws XMLStreamException, XMLBadFormatException, IOException
	{
		String result;

		result = source.replaceAll("[\t\n\r]", "");
		result = result.replaceAll(" *<", "<");

		//
		return result;
	}
}
