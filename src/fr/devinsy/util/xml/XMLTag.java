/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import javax.xml.namespace.QName;

/**
 * The Class XMLTag.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XMLTag
{
	public enum TagType
	{
		HEADER,
		START,
		END,
		EMPTY,
		CONTENT,
		FOOTER
	}

	private QName name;
	private TagType type;
	private XMLAttributes attributes;
	private String content;

	/**
	 * Instantiates a new XML tag.
	 * 
	 * @param name
	 *            the name
	 * @param type
	 *            the type
	 * @param attributes
	 *            the attributes
	 */
	public XMLTag(final QName name, final TagType type, final XMLAttributes attributes)
	{
		this.name = name;
		this.type = type;
		this.attributes = attributes;
		this.content = null;
	}

	/**
	 * Attributes.
	 * 
	 * @return the XML attributes
	 */
	public XMLAttributes attributes()
	{
		return this.attributes;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	public String getContent()
	{
		return this.content;
	}

	/**
	 * Gets the label.
	 * 
	 * @return the label
	 */
	public String getLabel()
	{
		String result;

		if (this.name == null)
		{
			result = "";
		}
		else
		{
			result = this.name.getLocalPart();
		}

		//
		return result;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public QName getName()
	{
		return this.name;
	}

	/**
	 * Gets the namespace URI.
	 * 
	 * @return the namespace URI
	 */
	public String getNamespaceURI()
	{
		String result;

		if (this.name == null)
		{
			result = "";
		}
		else
		{
			result = this.name.getNamespaceURI();
		}

		//
		return result;
	}

	/**
	 * Gets the prefix.
	 * 
	 * @return the prefix
	 */
	public String getPrefix()
	{
		String result;

		if (this.name == null)
		{
			result = "";
		}
		else
		{
			result = this.name.getPrefix();
		}

		//
		return result;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public TagType getType()
	{
		return this.type;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(final String content)
	{
		this.content = content;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(final QName name)
	{
		this.name = name;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(final TagType type)
	{
		this.type = type;
	}
}
