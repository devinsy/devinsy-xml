/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import fr.devinsy.util.strings.StringList;
import fr.devinsy.util.strings.StringListWriter;

/**
 * The Class XMLWriter.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */

public class XMLWriter
{
	protected PrintWriter out;

	/**
	 * Default constructor (useful for extend this class).
	 */
	protected XMLWriter()
	{
		this.out = null;
	}

	/**
	 * Initialize a XML Writer to a file.
	 * 
	 * @param target
	 *            Where write the XML data.
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public XMLWriter(final File target) throws UnsupportedEncodingException, FileNotFoundException
	{
		if (target == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(target), "UTF-8"));
		}
	}

	/**
	 * Initialize a XML Writer to a <code>OutputStream</code>.
	 * 
	 * @param target
	 *            Where write the XML data.
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 */
	public XMLWriter(final OutputStream target) throws UnsupportedEncodingException
	{
		if (target == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.out = new PrintWriter(new OutputStreamWriter(target, "UTF-8"));
		}
	}

	/**
	 * Instantiates a new XML writer.
	 * 
	 * @param target
	 *            the target
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 */
	public XMLWriter(final StringList target) throws UnsupportedEncodingException
	{
		if (target == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.out = new PrintWriter(new StringListWriter(target));
		}
	}

	/**
	 * Initialize a XML Writer to a <code>Writer</code>.
	 * 
	 * @param target
	 *            Where write the XML data.
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 */
	public XMLWriter(final Writer target) throws UnsupportedEncodingException
	{
		if (target == null)
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{

			this.out = new PrintWriter(target);
		}
	}

	/**
	 * This method closes the target stream.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void close() throws IOException
	{
		if (this.out != null)
		{
			this.out.flush();
			this.out.close();
		}
	}

	/**
	 * This method flushes the target stream.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void flush() throws IOException
	{
		if (this.out != null)
		{
			this.out.flush();
		}
	}

	/**
	 * This method writes a XML comment.
	 * 
	 * @param comment
	 *            The comment to write.
	 */
	public void writeComment(final String comment)
	{
		this.out.print("<!-- ");
		if (comment != null)
		{
			writeTagContent(comment);
		}
		this.out.print(" -->");
	}

	/**
	 * This method writes a XML tag with no content.
	 * 
	 * @param label
	 *            the label
	 * @param attributes
	 *            the attributes
	 */
	public void writeEmptyTag(final String label, final String... attributes)
	{
		writeEmptyTag(label, new XMLAttributes(attributes));
	}

	/**
	 * Write empty tag.
	 * 
	 * @param label
	 *            the label
	 * @param attributes
	 *            the attributes
	 */
	public void writeEmptyTag(final String label, final XMLAttributes attributes)
	{
		this.out.print("<");
		this.out.print(label);
		writeTagAttributes(attributes);
		this.out.print("/>");
	}

	/**
	 * This method writes a XML ender tag.
	 * 
	 * @param label
	 *            the label
	 */
	public void writeEndTag(final String label)
	{
		this.out.print("</");
		this.out.print(label);
		this.out.print(">");
	}

	/**
	 * This method writes a XML start tag.
	 * 
	 * @param label
	 *            the label
	 * @param attributes
	 *            the attributes
	 */
	public void writeStartTag(final String label, final String... attributes)
	{
		writeStartTag(label, new XMLAttributes(attributes));
	}

	/**
	 * Write start tag.
	 * 
	 * @param label
	 *            the label
	 * @param attributes
	 *            the attributes
	 */
	public void writeStartTag(final String label, final XMLAttributes attributes)
	{
		this.out.print("<");
		this.out.print(label);
		writeTagAttributes(attributes);
		this.out.print(">");
	}

	/**
	 * Write start tag.
	 * 
	 * @param tag
	 *            the tag
	 */
	public void writeStartTag(final XMLTag tag)
	{
		writeStartTag(tag.getLabel(), tag.attributes());
	}

	/**
	 * This method write a XML tag with attributes and boolean content data.
	 * 
	 * @param label
	 *            the label
	 * @param content
	 *            the content
	 * @param attributes
	 *            the attributes
	 */
	public void writeTag(final String label, final boolean content, final String... attributes)
	{
		writeTag(label, content, new XMLAttributes(attributes));
	}

	/**
	 * Write tag.
	 * 
	 * @param label
	 *            the label
	 * @param content
	 *            the content
	 * @param attributes
	 *            the attributes
	 */
	public void writeTag(final String label, final boolean content, final XMLAttributes attributes)
	{
		writeStartTag(label, attributes);
		writeTagContent(String.valueOf(content));
		writeEndTag(label);
	}

	/**
	 * This method write a XML tag with attributes and long content data.
	 * 
	 * @param label
	 *            the label
	 * @param content
	 *            the content
	 * @param attributes
	 *            the attributes
	 */
	public void writeTag(final String label, final long content, final String... attributes)
	{
		writeTag(label, content, new XMLAttributes(attributes));
	}

	/**
	 * Write tag.
	 * 
	 * @param label
	 *            the label
	 * @param content
	 *            the content
	 * @param attributes
	 *            the attributes
	 */
	public void writeTag(final String label, final long content, final XMLAttributes attributes)
	{
		writeStartTag(label, attributes);
		writeTagContent(String.valueOf(content));
		writeEndTag(label);
	}

	/**
	 * This method write a XML tag with attributes and content data. Content
	 * data are converted in XML format.
	 * 
	 * @param label
	 *            the label
	 * @param content
	 *            the content
	 * @param attributes
	 *            the attributes
	 */
	public void writeTag(final String label, final String content, final String... attributes)
	{
		writeTag(label, content, new XMLAttributes(attributes));
	}

	/**
	 * Write tag.
	 * 
	 * @param label
	 *            the label
	 * @param content
	 *            the content
	 * @param attributes
	 *            the attributes
	 */
	public void writeTag(final String label, final String content, final XMLAttributes attributes)
	{
		if (content == null)
		{
			writeEmptyTag(label, attributes);
		}
		else
		{
			writeStartTag(label, attributes);
			writeTagContent(content);
			writeEndTag(label);
		}
	}

	/**
	 * Write tag.
	 * 
	 * @param tag
	 *            the tag
	 */
	public void writeTag(final XMLTag tag)
	{
		switch (tag.getType())
		{
			case HEADER:
				writeXMLHeader(tag.attributes());
			break;
			case START:
				writeStartTag(tag.getLabel(), tag.attributes());
			break;
			case CONTENT:
				writeTag(tag.getLabel(), tag.getContent(), tag.attributes());
			break;
			case END:
				writeEndTag(tag.getLabel());
			break;
			case EMPTY:
				writeEmptyTag(tag.getLabel());
			break;
			case FOOTER:
			// TODO?
			break;
		}
	}

	/**
	 * This method writes attributes of a tag.
	 * 
	 * @param attributes
	 *            the attributes
	 */
	private void writeTagAttributes(final XMLAttributes attributes)
	{
		if ((attributes != null) && (!attributes.isEmpty()))
		{
			for (XMLAttribute attribute : attributes)
			{
				this.out.print(" ");
				this.out.print(attribute.getLabel());
				this.out.print("=\"");
				this.out.print(attribute.getValue());
				this.out.print("\"");
			}
		}
	}

	/**
	 * This method writes content using XML escape.
	 * 
	 * @param content
	 *            data to write in XML format.
	 */
	private void writeTagContent(final String content)
	{
		for (int count = 0; count < content.length(); count++)
		{
			char car = content.charAt(count);

			switch (car)
			{
				case '<':
					this.out.print("&lt;");
				break;
				case '>':
					this.out.print("&gt;");
				break;
				case '&':
					this.out.print("&amp;");
				break;
				case '"':
					this.out.print("&quot;");
				break;
				case '\'':
					this.out.print("&apos;");
				break;
				default:
					this.out.print(car);
			}
		}
	}

	/**
	 * This method writes a XML header with attributes.
	 * 
	 * @param attributes
	 *            the attributes
	 */
	public void writeXMLHeader(final String... attributes)
	{
		writeXMLHeader(new XMLAttributes(attributes));
	}

	/**
	 * This method writes a XML header with attributes.
	 * 
	 * @param attributes
	 *            the attributes
	 */
	public void writeXMLHeader(final XMLAttributes attributes)
	{
		//
		this.out.print("<?xml");

		//
		if ((attributes == null) || (!attributes.containsKey("version")))
		{
			this.out.print(" version=\"1.0\"");
		}

		//
		if ((attributes == null) || (!attributes.containsKey("encoding")))
		{
			this.out.print(" encoding=\"UTF-8\"");
		}

		//
		if ((attributes == null) || (!attributes.containsKey("standalone")))
		{
			this.out.print(" standalone=\"no\"");
		}

		//
		if (attributes != null)
		{
			for (XMLAttribute attribute : attributes)
			{
				this.out.print(" ");
				this.out.print(attribute.getLabel());
				this.out.print("=\"");
				this.out.print(attribute.getValue());
				this.out.print("\"");
			}
		}

		//
		this.out.print(" ?>");
	}
}
