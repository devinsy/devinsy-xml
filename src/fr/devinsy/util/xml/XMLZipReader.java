/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipInputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

/**
 * The Class XMLZipReader.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XMLZipReader extends XMLReader
{

	/**
	 * Instantiates a new XML zip reader.
	 * 
	 * @param file
	 *            the file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws XMLStreamException
	 *             the XML stream exception
	 */
	public XMLZipReader(final File file) throws IOException, XMLStreamException
	{
		super();

		XMLInputFactory factory = XMLInputFactory.newInstance();
		ZipInputStream zis = new ZipInputStream(new FileInputStream(file));
		zis.getNextEntry();
		this.in = factory.createXMLEventReader(zis, "UTF-8");
	}

	/**
	 * Instantiates a new XML zip reader.
	 * 
	 * @param source
	 *            the source
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws XMLStreamException
	 *             the XML stream exception
	 */
	public XMLZipReader(final InputStream source) throws IOException, XMLStreamException
	{
		super();

		XMLInputFactory factory = XMLInputFactory.newInstance();
		ZipInputStream zis = new ZipInputStream(source);
		zis.getNextEntry();
		this.in = factory.createXMLEventReader(zis, "UTF-8");
	}
}
