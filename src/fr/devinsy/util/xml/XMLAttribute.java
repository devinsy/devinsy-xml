/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class XMLAttribute.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XMLAttribute
{
	private String label;
	private String value;

	/**
	 * Instantiates a new XML attribute.
	 */
	public XMLAttribute()
	{
		this.label = null;
		this.value = null;
	}

	/**
	 * Instantiates a new XML attribute.
	 * 
	 * @param label
	 *            the label
	 * @param value
	 *            the value
	 */
	public XMLAttribute(final String label, final String value)
	{
		this.label = label;
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public XMLAttribute clone()
	{
		XMLAttribute result;

		result = new XMLAttribute(this.label, this.value);

		//
		return result;
	}

	/**
	 * Gets the label.
	 * 
	 * @return the label
	 */
	public String getLabel()
	{
		return this.label;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue()
	{
		return this.value;
	}

	/**
	 * Checks for equal value.
	 * 
	 * @param other
	 *            the other
	 * @return true, if successful
	 */
	public boolean hasEqualValue(final Object other)
	{
		boolean result;

		if (other == null)
		{
			result = false;
		}
		else if ((StringUtils.equals(this.label, ((XMLAttribute) other).label)) && (StringUtils.equals(this.value, ((XMLAttribute) other).value)))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Checks if is blank.
	 * 
	 * @return true, if is blank
	 */
	public boolean isBlank()
	{
		boolean result;

		if ((StringUtils.isBlank(this.label)) || (StringUtils.isBlank(this.value)))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Sets the label.
	 * 
	 * @param label
	 *            the new label
	 */
	public void setLabel(final String label)
	{
		this.label = label;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(final String value)
	{
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = String.format("%s=\"%s\"", this.label, this.value);

		//
		return result;
	}
}
