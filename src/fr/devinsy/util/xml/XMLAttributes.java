/*
 * Copyright (C) 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-xml.
 * 
 * Devinsy-xml is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-xml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-xml.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.stream.events.Attribute;

/**
 * The Class XMLAttributes.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XMLAttributes extends HashMap<String, XMLAttribute> implements Iterable<XMLAttribute>
{
	private static final long serialVersionUID = 8456469741805779474L;

	/**
	 * Instantiates a new XML attributes.
	 */
	public XMLAttributes()
	{
		super();
	}

	/**
	 * Instantiates a new XML attributes.
	 * 
	 * @param capacity
	 *            the capacity
	 */
	public XMLAttributes(final int capacity)
	{
		super(capacity);
	}

	/**
	 * Instantiates a new XML attributes.
	 * 
	 * @param source
	 *            the source
	 */
	public XMLAttributes(final Iterator<Attribute> source)
	{
		super();

		if (source != null)
		{
			while (source.hasNext())
			{
				Attribute attribute = source.next();

				add(new XMLAttribute(attribute.getName().getLocalPart(), attribute.getValue()));
			}
		}
	}

	/**
	 * Instantiates a new XML attributes from a string array. Strings are series
	 * of label and value. This constructor is a helper.
	 * 
	 * @param source
	 *            the source
	 */
	public XMLAttributes(final String... attributes)
	{
		super();

		if (attributes != null)
		{
			if (attributes.length % 2 == 0)
			{
				for (int count = 0; count < attributes.length; count += 2)
				{
					add(new XMLAttribute(attributes[count], attributes[count + 1]));
				}
			}
			else
			{
				throw new IllegalArgumentException("Parameter count is odd.");
			}
		}
	}

	/**
	 * Instantiates a new XML attributes.
	 * 
	 * @param source
	 *            the source
	 */
	public XMLAttributes(final XMLAttributes source)
	{
		super();

		addAll(source);
	}

	/**
	 * Adds the.
	 * 
	 * @param attribute
	 *            the attribute
	 */
	public void add(final XMLAttribute attribute)
	{
		if (attribute != null)
		{
			put(attribute.getLabel(), attribute);
		}
	}

	/**
	 * Adds the all.
	 * 
	 * @param source
	 *            the source
	 */
	public void addAll(final XMLAttributes source)
	{
		for (XMLAttribute attribute : source)
		{
			this.add(attribute);
		}
	}

	/**
	 * Gets the by label.
	 * 
	 * @param label
	 *            the label
	 * @return the by label
	 */
	public XMLAttribute getByLabel(final String label)
	{
		XMLAttribute result;

		result = get(label);

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<XMLAttribute> iterator()
	{
		Iterator<XMLAttribute> result;

		result = this.values().iterator();

		//
		return result;
	}

	/**
	 * Labels.
	 * 
	 * @return the sets the
	 */
	public Set<String> labels()
	{
		Set<String> result;

		result = this.keySet();

		//
		return result;
	}

	/**
	 * To array.
	 * 
	 * @return the XML attribute[]
	 */
	public XMLAttribute[] toArray()
	{
		XMLAttribute[] result;

		result = new XMLAttribute[size()];

		int count = 0;
		for (String key : this.keySet())
		{
			result[count] = get(key);
			count += 1;
		}

		//
		return result;
	}

	/**
	 * To list.
	 * 
	 * @return the list
	 */
	public List<XMLAttribute> toList()
	{
		List<XMLAttribute> result;

		result = new ArrayList<XMLAttribute>(values());

		//
		return result;
	}
}
